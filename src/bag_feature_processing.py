import os
import sys
import pickle
import timeit
import random
import numpy as np
import pandas as pd
import data_processing as dp


def get_histograms(bag_model, n_bag_features, photo_to_business_df, feature_vec_dir, dtype=np.float32):
    """
    This function creates a dictionary of histograms for all businesses included in the
    Pandas dataframe photo_to_business_df. Each histogram belongs to one unique business.
    It contains n_bag_features number of bins, and each bin contains a count of the number
    of images that have the bag-level feature that the bin represents. Bag-level features
    are simply an arbitrary number of clusters that the cluster model 'bag_model' learned
    from feature vectors extracted from all images in the given dataframe.
    :param bag_model: a clustering model that has been pre-trained to learn how to group
                      feature vectors in feature_vec_dir into n_bag_features number of
                      clusters; the model object must have a predict() method
    :param n_bag_features: the number of clusters that bag_model has been pre-trained to
                           discriminate
    :param photo_to_business_df: a Pandas dataframe that associates all image feature vectors
                                 in feature_vec_dir to business_id's; the dataframe must have
                                 'business_id' and 'photo_id' columns
    :param feature_vec_dir: a directory containing feature vectors of images specified in
                            photo_to_business_df; the dimension of each feature vector must be
                            compatible with bag_model
    :param dtype: the data type of all the histograms; default is numpy.float32
    :return: a dictionary whose keys are business id's and whose values are their corresponding
             histograms
    """
    print("Extracting business-level histograms")
    # business id's
    businesses = photo_to_business_df['business_id'].unique()
    # the dictionary to be returned
    business_histograms = {}
    # the count of how many feature vectors processed so far
    feature_vec_count = 0

    for index, business in enumerate(businesses):
        # all the photos belonging to the current business being processed
        photo_ids = photo_to_business_df[photo_to_business_df.business_id == business]['photo_id']
        # the histogram of the business
        bag_features = np.array([0] * n_bag_features, dtype=dtype)

        for i, photo_id in enumerate(photo_ids):
            # the feature vector that has been extracted from the current photo being processed
            feature_vec = np.load(os.path.join(feature_vec_dir, str(photo_id) + '.npy'))
            # count number of images belonging to cluster component 'prediction'
            prediction = bag_model.predict(feature_vec.reshape(1, -1))[0]
            bag_features[prediction] += 1

        # save the business-level features (count-based)
        business_histograms[business] = bag_features
        # print progress
        feature_vec_count += photo_ids.shape[0]
        sys.stdout.write(
            "\rProcessed business {}/{} -- Total feature vectors processed: {}".format(
                index + 1, len(businesses), feature_vec_count
            )
        )
        sys.stdout.flush()

    return business_histograms


def split_bag_data_into_training_and_validation(bag_features, bag_labels, ratio=0.15, dtype=np.float32):
    """

    :param bag_features:
    :param bag_labels:
    :param ratio:
    :param dtype:
    :return:
    """
    # the total number of feature vectors in all the histograms in bag_features
    n_feature_vectors = int(np.sum(bag_features))

    # the greatest lower bound of number of feature vectors to be "included" in the validation set
    n_validation_lower_bound = int(ratio * n_feature_vectors)

    # training histograms and corresponding labels to be returned
    training = bag_features
    training_labels = bag_labels

    # validation histograms and corresponding labels to be returned
    validation = np.empty((0, bag_features.shape[1]), dtype=dtype)
    validation_labels = np.empty((0, bag_labels.shape[1]), dtype=dtype)

    while True:
        # randomly pick a row
        pick_for_validation = np.random.randint(training.shape[0])
        # count total feature vectors
        feature_vector_count = int(np.sum(training[pick_for_validation]))
        # break if the sum of feature vectors included in validation is almost the required amount
        if int(np.sum(validation)) + feature_vector_count > n_validation_lower_bound:
            break

        # else, take two rows out of training and training labels and put them into training and training_labels
        validation = np.concatenate((validation, training[pick_for_validation].reshape(1, -1)), axis=0)
        validation_labels = np.concatenate(
            (validation_labels, training_labels[pick_for_validation].reshape(1, -1)), axis=0
        )
        training = np.delete(training, pick_for_validation, axis=0)
        training_labels = np.delete(training_labels, pick_for_validation, axis=0)

    difference = n_validation_lower_bound - int(np.sum(validation))
    if difference > 0:
        # pick the row in the remaining training such that the sum is the smallest that is at least >= difference
        remaining_sums = np.sum(training, axis=1, dtype=np.int)
        best_min_index = np.argwhere(remaining_sums == min(remaining_sums[remaining_sums >= difference]))[0]
        validation = np.concatenate((validation, training[best_min_index].reshape(1, -1)), axis=0)
        validation_labels = np.concatenate((validation_labels, training_labels[best_min_index].reshape(1, -1)), axis=0)
        training = np.delete(training, best_min_index, axis=0)
        training_labels = np.delete(training_labels, best_min_index, axis=0)

    assert int(np.sum(validation)) >= n_validation_lower_bound, "{} has not reach the lower bound {}".format(
        int(np.sum(validation)), n_validation_lower_bound
    )

    assert int(np.sum(training)) + int(np.sum(validation)) == n_feature_vectors, "inconsistent feature vector sums"

    print("Training shape: {}, dtype: {}".format(training.shape, training.dtype))
    print("Training labels shape: {}, dtype: {}".format(training_labels.shape, training_labels.dtype))
    print("Validation shape: {}, dtype: {}".format(validation.shape, validation.dtype))
    print("Validation labels shape: {}, dtype: {}".format(validation_labels.shape, validation_labels.dtype))
    print("Total feature vector count: {}".format(n_feature_vectors))
    print("Training feature vector count: {}".format(int(np.sum(training))))
    print("Validation feature vector count: {}".format(int(np.sum(validation))))
    print("Actual ratio: {}".format(int(np.sum(validation)) / n_feature_vectors))

    return training, training_labels, validation, validation_labels


if __name__ == '__main__':

    # """
    start_time = timeit.default_timer()
    N_COMPONENTS = (2048, )
    for n_components in N_COMPONENTS:
        print("Extracting business-level histograms ({} components)".format(n_components))
        with open(os.path.join(dp.MODEL_DIR, 'KMeans_' + str(n_components) + '_cc_inc_v3.pkl'), 'rb') as BAG_MODEL:
            BAG_MODEL = pickle.load(BAG_MODEL)

        print("KMeans inertia: {}".format(BAG_MODEL.inertia_))
        print("KMeans n_components: {}".format(BAG_MODEL.cluster_centers_.shape[0]))
        print("KMeans n_features: {}".format(BAG_MODEL.cluster_centers_.shape[1]))

        # train_mapping = pd.read_csv(os.path.join(dp.MAPPINGS_DIR_299, 'processed_train_mapping.csv'))
        test_mapping = pd.read_csv(os.path.join(dp.RAW_DIR, 'test_photo_to_biz.csv'))
        mapping = test_mapping
        print("Number of businesses: {}".format(mapping['business_id'].unique().shape[0]))
        print("Number of images: {}".format(mapping['photo_id'].unique().shape[0]))
        print("Number of feature vectors: {}".format(mapping['photo_id'].shape[0]))

        business_histograms = get_histograms(
            bag_model=BAG_MODEL,
            n_bag_features=BAG_MODEL.cluster_centers_.shape[0],
            photo_to_business_df=mapping,
            # feature_vec_dir=dp.TRANSFORMED_CENTER_CROPPED_299,
            feature_vec_dir=dp.TRANSFORMED_CENTER_CROPPED_299_TEST,
        )

        print()
        BUSINESS_HIST_FILE_PATH = os.path.join(
            # dp.HISTOGRAMS_DIR, 'business_histograms_' + str(n_components) + '_KMeans_1_cc_inc_v3.pkl'
            dp.HISTOGRAMS_DIR, 'test_business_histograms_' + str(n_components) + '_KMeans_cc_inc_v3.pkl'
        )
        with open(BUSINESS_HIST_FILE_PATH, 'wb') as hist_pickle:
            print("Saving count-based histograms at {}".format(BUSINESS_HIST_FILE_PATH))
            pickle.dump(business_histograms, hist_pickle)
        print("Done saving")
        print("Total time: {}s".format(timeit.default_timer() - start_time))
    # """

    """
    train_input = np.load(os.path.join(dp.TRAINING_SET_DIR, 'concatenated_train_input_KMeans_1_cc_inc_v3.npy'))
    train_labels = np.load(os.path.join(dp.TRAINING_SET_DIR, 'train_labels_KMeans_1_cc_inc_v3.npy'))

    sub_train, sub_train_labels, sub_validation, sub_validation_labels = \
        split_bag_data_into_training_and_validation(train_input, train_labels)

    print("Saving")
    np.save(os.path.join(dp.TRAINING_SET_DIR, 'sub_train_input_KMeans_1_cc_inc_v3.npy'), sub_train)
    np.save(os.path.join(dp.TRAINING_SET_DIR, 'sub_train_labels_KMeans_1_cc_inc_v3.npy'), sub_train_labels)
    np.save(os.path.join(dp.TRAINING_SET_DIR, 'sub_validation_KMeans_1_cc_inc_v3.npy'), sub_validation)
    np.save(os.path.join(dp.TRAINING_SET_DIR, 'sub_validation_labels_KMeans_1_cc_inc_v3.npy'), sub_validation_labels)
    print("Done saving")
    """
