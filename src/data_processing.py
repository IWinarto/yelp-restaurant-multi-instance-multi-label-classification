# Author:       Irsan Winarto
# Student I.D.: 44316983
# Supervisor:   Prof. Neil Bergmann
# Course:       ENGG4801

import os
import gc
import sys
import pandas as pd
import numpy as np
import image_processing as imp
# import matplotlib.pyplot as plt
import pickle
import timeit
from ast import literal_eval
from PIL import Image
from random import shuffle
from keras.applications.inception_v3 import InceptionV3
from keras.applications.inception_v3 import preprocess_input as inc_preproc
from keras.applications.mobilenet import MobileNet
from keras.applications.mobilenet import preprocess_input as mob_preproc
from keras.layers.core import Flatten
from keras.layers.pooling import MaxPooling2D
from keras.models import Model
from sklearn.decomposition import IncrementalPCA
from sklearn.decomposition import TruncatedSVD
from sklearn import preprocessing

# various paths
# DATA_DIR = os.path.join(os.getcwd().rsplit(os.sep, 2)[0], 'data')
MODEL_DIR = os.path.join(os.path.abspath(__file__).rsplit(os.sep, 2)[0], 'models')
DATA_DIR = os.path.join(os.path.abspath(__file__).rsplit(os.sep, 2)[0], 'data')
RAW_DIR = os.path.join(DATA_DIR, 'raw')
TRAIN_PHOTO_DIR = os.path.join(RAW_DIR, 'train_photos')
TEST_PHOTO_DIR = os.path.join(RAW_DIR, 'test_photos')
PROCESSED_PHOTOS_AS_ARRAYS_DIR = os.path.join(DATA_DIR, 'processed_photos_as_arrays')
PROCESSED_TRAINING_PHOTOS_DIR_224 = os.path.join(DATA_DIR, 'processed_training_photos_224')
PROCESSED_TRAINING_PHOTOS_DIR_299 = os.path.join(DATA_DIR, 'processed_training_photos_299')
TRANSFORMED_CENTER_CROPPED_299 = os.path.join(DATA_DIR, 'transformed_training_photos_299_center_cropped')
TRANSFORMED_CENTER_CROPPED_299_TEST = os.path.join(DATA_DIR, 'transformed_test_photos_299_center_cropped')
STACKED_FEATURE_VEC_DIR = os.path.join(DATA_DIR, 'stacked_feature_vectors')
CENTER_CROPPED_224 = os.path.join(DATA_DIR, 'processed_training_photos_224_center_cropped')
CENTER_CROPPED_299 = os.path.join(DATA_DIR, 'processed_training_photos_299_center_cropped')
CENTER_CROPPED_299_TEST = os.path.join(DATA_DIR, 'processed_test_photos_299_center_cropped')
MAPPINGS_DIR_224 = os.path.join(DATA_DIR, 'mappings_224')
MAPPINGS_DIR_299 = os.path.join(DATA_DIR, 'mappings_299')
TRAINING_SET_DIR = os.path.join(DATA_DIR, 'training_set')
TEST_SET_DIR = os.path.join(DATA_DIR, 'test_set')
HISTOGRAMS_DIR = os.path.join(DATA_DIR, 'histograms')


def find_orphan_images(file_list, df):
    """
    This function finds all the file names that are not in the data frame df.
    :param file_list: is a list of file names
    :param df: the first column of df must be photo_id which corresponds to
               file names without file extensions; all photo_id's must also be strings
    :return: a list of file names that are not mapped by the given dataframe df
    """
    # remove file extensions and convert the list to list of ints
    file_list = remove_extensions(file_list)
    file_list = pd.Series(list(map(int, file_list)))

    # convert the series into a list of photo id's
    photo_ids = list(df.photo_id)

    # find all file names in file_list that are in the dataframe df
    filtered = file_list.isin(photo_ids)
    indices = filtered[filtered == False].index

    # return a list of file names that are not in the dataframe df
    return list(file_list[indices])


def find_missing_images(df, file_list):
    """
    This function finds all photo_id's in the dataframe df that do not exist in
    file_list, a list of file names.
    :param df: the first column of the dataframe must be photo_id
    :param file_list: this is a list of file names
    :return: a list of photo_id's that do not exist in file_list
    """
    # remove file extensions and convert the file names into ints
    file_list = remove_extensions(file_list)
    file_list = list(map(int, file_list))

    # find all photo id's in df that are also in file_list
    filtered = df.photo_id.isin(file_list)
    indices = filtered[filtered == False].index

    # return a list of photo_id's that do not belong in file list
    return list(df.photo_id[indices])


def remove_extensions(file_list):
    """
    This function removes all the extensions of the file names in file_list.
    :param file_list: must be a list of strings
    :return: a new list whose entries are the same as file_list such that the file
             extensions have been removed. The order of the entries is preserved.
    """
    new_list = []
    new_list.extend(file_list)
    for index, file_name in enumerate(new_list):
        new_list[index] = file_name.split(sep='.')[0]

    return new_list


def process_images(final_size, source_path, save_path, format='JPEG'):
    """
    This function calls image_processing.resize_shorter_size_and_center_crop() on
    all image files in the source_path. All outputs are saved in the save_path.
    The resulting images are saved in the format specified, and have the specified
    final size.
    :param final_size: the size of the resulting images
    :param source_path: the directory of where images to be processed are stored
    :param save_path: the directory of where output images are stored
    :param format: the format of the processed images
    """
    # get all images in the source path
    image_list = os.listdir(source_path)
    # count the number of images
    image_count = len(image_list)

    # process all the images
    for index, image_file in enumerate(image_list):
        # print the progress
        sys.stdout.write('\rProcessing {}/{}'.format(index + 1, image_count))
        sys.stdout.flush()

        # open the image file, process it, and save it in the save_path
        image = Image.open(os.path.join(source_path, image_file))
        image = imp.resize_shorter_side_and_center_crop(image, final_size)
        image.save(os.path.join(save_path, image_file), format)


def one_hot_encode_tags(tags):
    """
    This function one-hot encodes the tags as a sequence of 1 and 0 ints.
    :param tags: is a string of numbers between 0 and 8 inclusive and
                 may contain space characters
    :return: one-hot encoding of the tags
    """
    # convert the tags specified to a bitfield such that the most
    # significant digit corresponds to class 0, the next significant digit
    # corresponds to class 1, and so on.
    try:
        tags = tags.replace(' ', '')
    except AttributeError:
        print(tags)

    bitfields = [
        1 if str(i) in tags else 0
        for i in range(9)
    ]

    return bitfields


def sample_businesses(data_df, training_portion=0.8):
    """
    This function divides the original training set specified in
    train_photo_to_biz_ids.csv into training and test sets.
    :param data_df: the data frame containing business id's and photo id's
    :param training_portion: the percentage of the total businesses to
                             be sampled as a training set
    :return: a list of business id's for training and testing
    """
    # get the mapping and count the number of images each business has
    img_counts = data_df.groupby(['business_id']).count()
    img_counts.columns = ['count']

    # the maximum number of images for training
    max_img_count = int(data_df.shape[0] * training_portion)
    # the actual total number of images for training
    total_count = 0
    # a list of business id's for training
    train_businesses = []

    while total_count < max_img_count and not img_counts.empty:
        # get business randomly while total images still below threshold
        business_id = img_counts.index[np.random.randint(0, img_counts.shape[0])]
        count = img_counts.loc[business_id, 'count']

        if count + total_count > max_img_count:
            # get the business with minimum amount of images to be included in training
            count = min(img_counts['count'])
            filter_min = img_counts['count'] == count
            business_id = img_counts.loc[filter_min].index.tolist()[0]

        # include the business, update count, and drop the business from the pool
        train_businesses.append(business_id)
        total_count += count
        img_counts = img_counts.drop(business_id)

    total_business_count = data_df['business_id'].unique().shape[0]

    # the list of business id's for testing
    test_businesses = img_counts.loc[~img_counts.index.isin(train_businesses)]
    test_businesses = test_businesses.index.tolist()

    assert len(train_businesses) + len(test_businesses) == total_business_count,  \
        "number of training {} and testing {} combined do not match {}".format(
            len(train_businesses), len(test_businesses), img_counts.shape[0]
        )

    return [train_businesses, test_businesses]


def assign_tags(df):
    """
    This function assigns tags to the businesses in the given df
    based on the tag mapping provided by train.csv.
    :param df: the table of businesses to be tagged
    :return: the tagged version of the given df
    """
    # assign tags to businesses
    tag_df = pd.read_csv(os.path.join(RAW_DIR, 'train.csv'))
    df = pd.merge(df, tag_df, on='business_id', how='inner')

    assert np.nan not in df['labels'].tolist(), \
        "Woops, some business id's do not have tags!!!"

    # one hot encode the tags
    df['one_hot_tags'] = pd.Series({
        index: one_hot_encode_tags(row['labels'])
        for index, row in df.iterrows()
    })

    assert np.nan not in df['one_hot_tags'].tolist(), \
        "Woops, not all tags got encoded!!!"

    return df


def generate_train_and_test_sets(train_file_path, test_file_path, data_size, training_portion=0.8):
    """
    This function generates two tables using train_photo_to_biz_ids.csv:
        (1) a csv file that maps training images to business id's, and
        (2) a csv file that maps test images to business id's.
    :param train_file_path: the csv mapping file for training businesses
    :param test_file_path: the csv mapping file for testing businesses
    :param data_size: is the total size of training and test data sets
    :param training_portion: a portion of the original csv rows to be included
                             in the training set
    """
    # the original data mapping
    data_df = pd.read_csv(os.path.join(RAW_DIR, 'train_photo_to_biz_ids.csv'))
    data_df = data_df.sort_values('business_id')

    if data_size <= 0 or data_size > data_df.shape[0]:
        raise Exception(
            'data_size must be a positive integer no greater than the number of rows '
            'in train_photo_to_biz_ids.csv'
        )

    # randomly pick a contiguous chunk of data_size rows
    random_first_idx = np.random.randint(0, data_df.shape[0] - data_size + 1)
    last_idx = random_first_idx + data_size
    data_df = data_df.iloc[random_first_idx: last_idx, :]

    # get business id's for training and test sets
    train_businesses, test_businesses = sample_businesses(data_df, training_portion)

    # extract data frames for training and testing from the original training set
    # df = pd.read_csv(os.path.join(RAW_DIR, 'train_photo_to_biz_ids.csv'))
    train_df = data_df.loc[data_df['business_id'].isin(train_businesses)]
    test_df = data_df.loc[data_df['business_id'].isin(test_businesses)]

    assert train_df.shape[0] + test_df.shape[0] == data_size, \
        "number of training {} and test {} do not match original {}".format(
            train_df.shape[0], test_df.shape[0], data_size
        )

    train_df = assign_tags(train_df)
    test_df = assign_tags(test_df)

    print(train_df.head())
    print("length: {}".format(train_df.shape[0]))
    print(test_df.head())
    print("length: {}".format(test_df.shape[0]))

    train_df.to_csv(train_file_path, index=False)
    test_df.to_csv(test_file_path, index=False)


def generate_train_and_validation_sets(train_file_path, validation_file_path, data_size, training_portion=0.8):
    """
    This is a wrapper function that "renames" / calls generate_train_and_test_sets() to generate training and validation
    sets.
    :param train_file_path: the csv mapping file for training businesses
    :param validation_file_path: the csv mapping file for validation businesses
    :param data_size: the total size of training and validation sets
    :param training_portion: a proportion of the original csv rows to be included in the training set
    """
    generate_train_and_test_sets(train_file_path, validation_file_path, data_size, training_portion=training_portion)


def combine_data(directory, base, n):
    """
    This function concatenates n number of numpy arrays from the files with file name patterns base_x and base_tags_x,
    where x is a number in range(n). The files must be found in the given directory. This function then produces two
    concatenated numpy arrays, which will be saved in the same directory with the file names base.npy and base_tags.npy.
    :param directory: the directory where the .npy files are to be found and where the resulting .npy files are to be
                      saved
    :param base: file names must have base_x and base_tags_x patterns, where x is in range(n)
    :param n: the number of "base" files to be concatenated
    """
    # the resulting concatenated arrays to be saved
    train = np.empty((0, 224, 224, 3), dtype=np.float16)
    tags = np.empty((0, 511), dtype=np.int16)

    for j in range(n):
        data_file = os.path.join(directory, base + str(j) + '.npy')
        tags_file = os.path.join(directory, base + 'tags_' + str(j) + '.npy')

        print("concatenating ", data_file, " and ", tags_file)
        train = np.concatenate((train, np.load(data_file)), axis=0)
        tags = np.concatenate((tags, np.load(tags_file)), axis=0)

    np.save(os.path.join(directory, base[:-1] + '.npy'), train)
    np.save(os.path.join(directory, base + 'tags.npy'), tags)

    # free up memory since the files are expected to be large in size
    del train
    del tags
    gc.collect()


def image_batch_generator(image_dir, train_mapping, preprocess, size=2**8, dtype=np.float32, shape=(224, 224, 3),
                          randomise=True, verbose=False):
    """
    This generator yields a batch of images from the given directory containing the images. The number of images in the
    batch is equal to the given size. The batch is a numpy array of type dtype.
    :param size: the number of images in a single batch
    :param image_dir: the directory containing images that are used to generate numpy batches
    :param preprocess: each data array gets processed by the 'preprocess' function
    :param dtype: the data type of the image batch
    :param shape: the dimensions of the data to be concatenated; default is (224, 224, 3)
    :param train_mapping: this is the csv file that maps photos to businesses and labels
    :param randomise: if True (default), it will shuffle the list of data files to be yielded
    :param verbose: if True, this function prints out the index of the data being concatenated; default is False
    :return: a numpy array that contains RGB images of length "size" and of type "dtype"
    """
    photo_ids = pd.read_csv(train_mapping)
    photo_ids = photo_ids['photo_id']
    # the 299 x 299 RGB training images
    image_files_299 = [
        str(photo_id) + '.jpg'
        for photo_id in photo_ids
    ]

    if randomise:
        shuffle(image_files_299)

    # for generating current batch
    next_start = 0

    while next_start < len(image_files_299):
        # the final index
        limit_index = min(next_start + size, len(image_files_299))

        # image batch as numpy array of np.float32 elements
        image_array = np.empty(tuple([limit_index - next_start]) + shape, dtype=dtype)

        print("_" * 80)
        sys.stdout.write("Concatenating {} to {}".format(next_start, limit_index - 1))
        sys.stdout.flush()

        # concatenate the images into a single numpy array of np.float32 elements
        for index in range(next_start, limit_index):
            if verbose:
                sys.stdout.write("\rConcatenating {}".format(index))
                sys.stdout.flush()
            image_file = image_files_299[index]
            image_file = os.path.join(image_dir, image_file)
            with Image.open(image_file) as image:
                image = np.array(image, dtype=dtype)
                image = preprocess(image)
                image_array[index - next_start] = image

        # get the next start index
        next_start = limit_index

        # return it
        print("\nShape of image_array is {}, dtype is {}".format(image_array.shape, image_array.dtype))
        done = yield image_array

        # free up memory
        del image_array
        gc.collect()

        if done:
            break


def transform_dataset(image_list, image_dir, dest_dir, feature_means, model, preprocess, transformer,
                      ext='jpg', dtype=np.float32, verbose=False):
    """
    This function transforms every image in the image_list contained in image_dir by the following processes:
        1. each image is converted into a numpy array and processed by the given preprocess() function,
        2. the numpy array is then fed through the given model to extract features of the image,
        3. the resulting feature vector of the image is then zero-centered with the given means and normalised,
        4. the function transformer.transform() is then called on the normalised feature vector, and
        5. finally, the resulting transformed feature vector is saved with the same filename as the original image, but
           with the extension .npy, in the given directory dest_dir. The object is saved as type dtype.
    :param image_list: a list of image file names; they must be convertible into strings
    :param image_dir: the directory where the image files (must be .jpg) to be transformed are kept in
    :param dest_dir: the destination directory where transformed feature vectors are to be saved in
    :param feature_means: the mean feature vector of all feature vectors of images in image_list extracted by the model
    :param model: the model object ith predict() function used for image feature extraction
    :param preprocess: the preprocess() function used for preprocessing images in step 1
    :param transformer: the transformer object with transform() method used for step 4
    :param ext: the image file extension e.g. 'jpg', 'bmp', etc. An extra '.' is prefixed to the extension.
    :param dtype: the data type of the numpy arrays of images; default is np.float32
    :param verbose: if this is False, this function will only print number of images processed
    """
    print('_' * 80)

    for index, image_file in enumerate(image_list):
        sys.stdout.write("\rTransforming {}/{}".format(index + 1, len(image_list)))
        sys.stdout.flush()
        # get the file name without the extension
        image_file = str(image_file).split('.')[0]
        # the file name of the transformed feature vector to be saved
        transformed_feature_file_name = image_file + '.npy'
        # the file name of the image whose features are to be transformed
        image_file = image_file + '.' + ext
        image_file = os.path.join(image_dir, image_file)

        with Image.open(image_file) as image:
            image = np.array(image, dtype=dtype)
            # preprocess as required by the model
            image_array = preprocess(image)
            # process through the pretrained network
            features = model.predict(np.expand_dims(image_array, axis=0), batch_size=1, verbose=0)
            # zero-centered and normalise
            features = features - feature_means
            features = preprocessing.normalize(features, copy=False, norm='l2')
            # reduce dimension
            features = transformer.transform(features).astype(np.float32, copy=False)
            # done with the current image
            np.save(os.path.join(dest_dir, transformed_feature_file_name), features.astype(dtype))
            del image_array
            del features
            gc.collect()


def feature_batch_generator(model, size, model_batch_size, image_batch_generator_arguments,
                            model_verbose=1, dtype=np.float32):
    """
    This generator calls image_batch_generator and processes the output by calling model.predict() on it and yields the
    result back. This generator stops yielding when image_batch_generator() stops yielding. Alternatively, send False to
    this generator to stop image_batch_generator from yielding and, therefore, this generator too.
    :param model: the model through which the output of image_batch_generator gets processed
    :param image_batch_generator_arguments: a dictionary of image_batch_generator() arguments
    :param size: size is the number of rows in the feature_batch to be yielded. It is recommended that size is an
                 integer multiply of the size argument of image_batch_generator()
    :param model_batch_size: is equivalent to the batch_size param of the given Keras model
    :param model_verbose: is equivalent to the verbose param of the given Keras model; default is 1
    :parma dtype: is the data type; default is numpy.float32
    """
    generator = image_batch_generator(**image_batch_generator_arguments)

    while True:
        try:
            feature_batch = np.empty((0, model.output_shape[1]), dtype=dtype)
            num_feature_vectors = feature_batch.shape[0]
            print("feature_batch shape: {}, dtype: {}".format(feature_batch.shape, feature_batch.dtype))

            for image_batch in generator:
                # get the image batch features
                print("image_batch shape: {}, dtype: {}".format(image_batch.shape, image_batch.dtype))
                features = model.predict(image_batch, batch_size=model_batch_size, verbose=model_verbose)
                print("features shape: {}, dtype: {}".format(features.shape, features.dtype))
                # copy the features into the feature_batch
                feature_batch = np.concatenate((feature_batch, features), axis=0)
                print("feature_batch shape: {}, dtype: {}".format(feature_batch.shape, feature_batch.dtype))
                # free up memory
                del features
                del image_batch
                gc.collect()

                # if the feature batch size is met, yield the batch
                if feature_batch.shape[0] >= size:
                    break

            if feature_batch.shape[0] > 0:
                done = yield feature_batch
                num_feature_vectors = feature_batch.shape[0]
                # free up memory
                del feature_batch
                gc.collect()

            # done is specified or the last batch of images have been processed, finish yielding
            if done or num_feature_vectors == 0 or num_feature_vectors < size:
                # this should throw StopIteration
                generator.send(True)
                return

        except StopIteration:
            return


def concatenate_histograms(business_id, histogram_dicts, concatenation_length, dtype=np.float32):
    """
    This function takes a list of dictionaries of histograms and concatenate the values, which are histograms, of the
    dictionaries associated to the key business_id, length-wise.
    :param business_id: the key of all dictionaries whose values/histograms are to be concatenated length-wise
    :param histogram_dicts: a list of dictionaries of histograms
    :param concatenation_length: the total length of the concatenation
    :param dtype: the data type of the concatenation
    :return: a numpy array of concatenated histograms as selected from histogram_dicts using the key business_id
    """
    # the resulting numpy array to be returned
    concatenated_hist = np.empty((concatenation_length, ), dtype=dtype)
    # the index where to concatenate each histogram in histogram_dicts referenced by business_id
    next_start = 0
    # concatenate
    for dictionary in histogram_dicts:
        histogram = dictionary[business_id]
        concatenated_hist[next_start: next_start + histogram.shape[0]] = histogram
        next_start = next_start + histogram.shape[0]

    # sanity check
    next_start = 0
    for dictionary in histogram_dicts:
        length = dictionary[business_id].shape[0]
        assert np.array_equal(concatenated_hist[next_start: next_start + length], dictionary[business_id]), \
            "concatenation mismatch"
        next_start = next_start + length

    return concatenated_hist


def make_training_set(histogram_dicts, biz_and_tags_df, dtype=np.float32):
    """
    This function creates a training input numpy array and the corresponding training output/labels numpy array.
    :param histogram_dicts: a list of dictionary objects whose keys must be business_id's in biz_and_tags_df and whose
                            values are the histograms associated to the keys
    :param biz_and_tags_df: a Pandas dataframe with columns 'business_id' and 'one_hot_tags' such that each unique
                            business_id must have a unique one_hot_tags
    :param dtype: the data type of the training input and labels to be returned; default is numpy.float32
    :return: a training input in the form of numpy array such that each row is a concatenation of histograms associated
             to the same business_id (key) in histogram_dicts, and a numpy array of labels associated to each row of the
             training input array
    """
    print("Concatenating histograms")
    concatenation_length = 0
    for dictionary in histogram_dicts:
        concatenation_length += list(dictionary.values())[0].shape[0]
    print("Concatenation length: {}".format(concatenation_length))

    biz_and_tags_df = biz_and_tags_df[['business_id', 'one_hot_tags']].drop_duplicates()
    number_of_businesses = biz_and_tags_df.shape[0]
    print("Number of unique businesses: {}".format(number_of_businesses))

    assert len(histogram_dicts[0]) == number_of_businesses, \
        "length of mapping {} != length of histograms {}".format(number_of_businesses, len(histogram_dicts[0]))

    train_features = np.empty((number_of_businesses, concatenation_length), dtype=dtype)
    train_labels = np.empty((number_of_businesses, 9), dtype=dtype)

    for index, row in enumerate(biz_and_tags_df.iterrows()):
        print("Extracting {}/{}".format(index + 1, biz_and_tags_df.shape[0]))
        business_id = row[1]['business_id']

        train_features[index] = concatenate_histograms(business_id, histogram_dicts, concatenation_length)
        train_labels[index] = np.array(literal_eval(row[1]['one_hot_tags']), dtype=np.float32)

    print("training input shape: {}, dtype: {}".format(train_features.shape, train_features.dtype))
    print("training labels shape: {}, dtype: {}".format(train_labels.shape, train_labels.dtype))

    return train_features, train_labels


def make_test_set(histogram_dicts, dtype=np.float32):
    """
    This function creates a test input numpy array of histograms for all businesses in the test set.
    :param histogram_dicts: a list of dictionaries of histograms of businesses in the test set ; it is a requirement
                            that the key sets of all the dictionaries are equal and that every key is a business id
    :param dtype: the data type of the numpy array of concatenated histograms to be returned
    :return: a numpy array where each row corresponds to exactly one unique business and the row is a concatenation of
             all the histograms that describe the business
    """
    print("Concatenating histograms")
    concatenation_length = 0
    for dictionary in histogram_dicts:
        concatenation_length += list(dictionary.values())[0].shape[0]
    print("Concatenation length: {}".format(concatenation_length))

    business_ids = list(histogram_dicts[0].keys())
    number_of_businesses = len(business_ids)
    print("Number of businesses: {}".format(len(business_ids)))

    test_features = np.empty((number_of_businesses, concatenation_length), dtype=dtype)
    test_business_ids = [''] * number_of_businesses

    for index, business_id in enumerate(business_ids):
        print("Extracting {}/{}".format(index + 1, number_of_businesses))
        test_features[index] = concatenate_histograms(business_id, histogram_dicts, concatenation_length)
        test_business_ids[index] = business_id

    assert '' not in test_business_ids, "there's an empty business id string"

    print("test input shape: {}, dtype: {}".format(test_features.shape, test_features.dtype))
    print("corresponding test buisness id's length: {}".format(len(test_business_ids)))

    return test_features, test_business_ids


if __name__ == '__main__':
    # create a directory for mapping csv files for 299 x 299 images
    """
    if not os.path.exists(MAPPINGS_DIR_299):
        print("Creating directory: {}".format(MAPPINGS_DIR_299))
        os.makedirs(MAPPINGS_DIR_299)
    """

    """
    if not os.path.exists(MAPPINGS_DIR_224):
        print("Creating directory: {}".format(MAPPINGS_DIR_224))
        os.makedirs(MAPPINGS_DIR_224)
    """

    # the mapping for training data set
    train_file_path = os.path.join(MAPPINGS_DIR_299, 'processed_train_mapping.csv')
    # this will be empty (Keras will auto split the training set into training and validation)
    validation_file_path = os.path.join(MAPPINGS_DIR_299, 'processed_validation_mapping.csv')
    # the mapping for testing data set
    test_file_path = os.path.join(RAW_DIR, 'test_photo_to_biz.csv')

    """
    # the mapping for training data set
    train_file_path = os.path.join(MAPPINGS_DIR_224, 'processed_train_mapping.csv')
    # this will be empty (Keras will auto split the training set into training and validation)
    validation_file_path = os.path.join(MAPPINGS_DIR_224, 'processed_validation_mapping.csv')
    # the mapping for testing data set
    test_file_path = os.path.join(MAPPINGS_DIR_224, 'processed_test_mapping.csv')
    """

    """
    # generate training and validation mappings
    if not os.path.exists(train_file_path):
        print("Making csv mappings")
        generate_train_and_validation_sets(train_file_path, validation_file_path, 234842, training_portion=1)
        # generate_train_and_test_sets(train_file_path, test_file_path, 150000)
    """

    """
    print("Processing image array through MobileNet")
    mob = MobileNet(include_top=False, input_shape=(224, 224, 3))
    for layer in mob.layers:
        layer.trainable = False
    x = Flatten()(mob.output)
    model = Model(inputs=mob.input, outputs=x)
    model.summary()
    """

    """
    print("Processing image array through Inception V3")
    SHAPE = (299, 299, 3)
    inc_V3 = InceptionV3(include_top=False, input_shape=SHAPE)
    for layer in inc_V3.layers:
        layer.trainable = False
    x = MaxPooling2D()(inc_V3.output)
    x = Flatten()(x)
    model = Model(inputs=inc_V3.input, outputs=x)
    model.summary()
    """

    """
    SIZE = 4096
    generator = image_batch_generator(CENTER_CROPPED_299, train_file_path, inc_preproc, size=SIZE, shape=SHAPE)

    length = 0
    mean = np.zeros((model.output_shape[1],), dtype=np.float32)

    for image_array in generator:
        batch = model.predict(image_array, batch_size=256, verbose=1)
        print("Batch shape: {}, dtype: {}".format(batch.shape, batch.dtype))
        mean = ((mean * length) + (np.mean(batch, axis=0) * batch.shape[0])) / (length + batch.shape[0])
        length = length + batch.shape[0]
        print("mean shape: {}, dtype: {}".format(mean.shape, mean.dtype))
        del batch
        del image_array
        gc.collect()

    np.save(os.path.join(DATA_DIR, 'center_cropped_batch_means_inc_V3.npy'), mean)
    """

    """
    # incremental PCA to reduce dimension of feature extraction
    N_COMPONENTS = 4096
    inc_PCA = IncrementalPCA(n_components=N_COMPONENTS)

    # feature batch size
    SIZE = 2**14

    # process the images through Mobile Net
    kwargs = {
        'image_dir': CENTER_CROPPED_299,
        'train_mapping': train_file_path,
        'preprocess': inc_preproc,
        'size': 4096,
        'shape': SHAPE
    }
    generator = feature_batch_generator(model, SIZE, 256, kwargs)

    total_processed = 0
    processed_batch_sizes = []
    total_variances = []
    mean = np.load(os.path.join(DATA_DIR, 'center_cropped_batch_means_inc_V3.npy')).astype(np.float32)
    # timing
    start_time = timeit.default_timer()
    previous_time = start_time
    for feature_batch in generator:
        print("feature_batch shape: {}, dtype: {}".format(feature_batch.shape, feature_batch.dtype))
        feature_batch = feature_batch - mean
        feature_batch = preprocessing.normalize(feature_batch, copy=False, norm='l2')
        print("feature_batch shape: {}, dtype: {}".format(feature_batch.shape, feature_batch.dtype))
        inc_PCA.partial_fit(feature_batch)
        # print timing
        print("time taken to fit: {}s, elapsed time: {}s"
              .format(timeit.default_timer() - previous_time, timeit.default_timer() - start_time))
        previous_time = timeit.default_timer()
        # update stats
        total_variance = inc_PCA.explained_variance_ratio_.sum()
        total_variances.append(total_variance)
        total_processed += feature_batch.shape[0]
        processed_batch_sizes.append(total_processed)
        print("Total images: {} - Total variance explained: {}".format(total_processed, total_variance))
        # save the PCA
        print("Saving inc_PCA")
        inc_PCA_pickle = open(os.path.join(MODEL_DIR, 'inc_V3_cc_inc_PCA.pkl'), 'wb')
        pickle.dump(inc_PCA, inc_PCA_pickle)
        inc_PCA_pickle.close()

    # save the variances and batch sizes processed
    np.save(os.path.join(DATA_DIR, 'inc_V3_cc_variances_history.npy'), np.array(total_variances))
    np.save(os.path.join(DATA_DIR, 'inc_V3_cc_batch_sizes_processed_history.npy'), np.array(processed_batch_sizes))

    # plot the total variances explained
    plt.plot(processed_batch_sizes, total_variances, 'bo')
    plt.plot([0, 250000], [0.83, 0.83], linestyle='-', color='r')
    plt.title('Incremental PCA (N_components = {})'.format(4096))
    plt.ylabel('Total Variance Explained')
    plt.xlabel('Feture Vectors Processed')
    plt.show()
    fig_save_path = os.path.join(DATA_DIR, 'inc_V3_cc_inc_PCA.png')
    plt.savefig(fig_save_path, dpi=1200)
    """

    """
    # transform feature vectors
    # if not os.path.exists(TRANSFORMED_CENTER_CROPPED_299):
    if not os.path.exists(TRANSFORMED_CENTER_CROPPED_299_TEST):
        print("Creating directory for transformed feature vectors")
        # os.makedirs(TRANSFORMED_CENTER_CROPPED_299)
        os.makedirs(TRANSFORMED_CENTER_CROPPED_299_TEST)

    with open(os.path.join(MODEL_DIR, 'inc_V3_cc_inc_PCA.pkl'), 'rb') as inc_PCA_pickle:
        inc_PCA = pickle.load(inc_PCA_pickle)
    print("Total variance explained: {}".format(inc_PCA.explained_variance_ratio_.sum()))

    # image_ids = pd.read_csv(train_file_path)['photo_id']
    image_ids = pd.read_csv(test_file_path)['photo_id'].unique()
    means = np.load(os.path.join(DATA_DIR, 'center_cropped_batch_means_inc_V3.npy')).astype(np.float32)
    transform_dataset(
        image_list=image_ids,
        # image_dir=CENTER_CROPPED_299,
        image_dir=CENTER_CROPPED_299_TEST,
        dest_dir=TRANSFORMED_CENTER_CROPPED_299_TEST,
        feature_means=means,
        model=model,
        preprocess=inc_preproc,
        transformer=inc_PCA
    )
    """

    """
    print("Stacking feature vectors")
    feature_vector_files = os.listdir(TRANSFORMED_CENTER_CROPPED_299)
    length = len(feature_vector_files)
    print("Total number of vectors: {}".format(length))
    stacked_vectors = np.empty((length, 4096), dtype=np.float32)
    for index, file in enumerate(feature_vector_files):
        sys.stdout.write("\r{}/{}".format(index + 1, length))
        sys.stdout.flush()
        vector = np.load(os.path.join(TRANSFORMED_CENTER_CROPPED_299, file))
        stacked_vectors[index] = vector

    print('\nSaving stacked vectors')
    STACKED_VECTORS_FILE_NAME = os.path.join(DATA_DIR, 'transformed_features_stacked_cc_inc_v3.npy')
    np.save(STACKED_VECTORS_FILE_NAME, stacked_vectors)
    """

    # """
    # print("Making training set")
    print("Making test set")
    DIMENSIONS = [64, 128, 256, 512, 1024, 2048]

    HIST_FILE_NAMES = [
        # 'business_histograms_' + str(dimension) + '_KMeans_cc_inc_v3.pkl'
        'test_business_histograms_' + str(dimension) + '_KMeans_cc_inc_v3.pkl'
        for dimension in DIMENSIONS
    ]

    NEXT_HIST_NAMES = [
        # 'business_histograms_' + str(dimension) + '_KMeans_1_cc_inc_v3.pkl'
        'test_business_histograms_' + str(dimension) + '_KMeans_1_cc_inc_v3.pkl'
        for dimension in DIMENSIONS
    ]

    HIST_FILE_NAMES.extend(NEXT_HIST_NAMES)

    HISTOGRAMS_DICTS = []
    for histogram_file in HIST_FILE_NAMES:
        with open(os.path.join(HISTOGRAMS_DIR, histogram_file), 'rb') as hist_pickle:
            hist_dict = pickle.load(hist_pickle)
        HISTOGRAMS_DICTS.append(hist_dict)
        print(
            "Length of histogram dictionary: {}, hist_shape: {}".format(
                len(hist_dict), list(hist_dict.values())[0].shape
            )
        )

    for hist_pair in zip(HISTOGRAMS_DICTS[:-1], HISTOGRAMS_DICTS[1:]):
        assert len(hist_pair[0]) == len(hist_pair[1]), "Not all histogram dictionaries have the same length"
        assert set(hist_pair[0].keys()) == set(hist_pair[1].keys()), "Business id's are not the same"

    print("Length of all histogram dictionaries: {}".format(len(HISTOGRAMS_DICTS[0])))

    # the Pandas dataframe containing business_id's and labels
    # train_df = pd.read_csv(os.path.join(MAPPINGS_DIR_299, 'processed_train_mapping.csv'))
    # make training set
    # train_features, train_labels = make_training_set(HISTOGRAMS_DICTS, train_df)

    # make test set
    test_features, test_business_ids = make_test_set(HISTOGRAMS_DICTS)

    # print("Saving training input")
    # np.save(os.path.join(TRAINING_SET_DIR, 'concatenated_train_input_KMeans_1_cc_inc_v3.npy'), train_features)
    # print("Saving corresponding output labels")
    # np.save(os.path.join(TRAINING_SET_DIR, 'train_labels_KMeans_1_cc_inc_v3.npy'), train_labels)

    print("Saving test input")
    np.save(os.path.join(TEST_SET_DIR, 'concatenated_test_input_KMeans_1_cc_inc_v3.npy'), test_features)
    print("Saving corresponding test business ids")
    with open(os.path.join(TEST_SET_DIR, 'corresponding_test_business_ids_1.pkl'), 'wb') as ids_pickle:
        pickle.dump(test_business_ids, ids_pickle)

    print("Done saving")
    # """
