import os, gc, pickle
import data_processing as dp
import numpy as np
from collections import defaultdict
from itertools import cycle


def print_section_header(message):
    """
    This function places "=" * len(message) above and below the given message.
    :param message: the given message to be "decorated"
    """
    print()
    print("=" * len(message))
    print(message)
    print("=" * len(message))


def train_model(model, n_epochs, batch_size, n_chunks=1, validation_split=0.0):
    """
    This function trains the given Keras model.
    :param model: the Keras model to be trained
    :param n_epochs: the number of epochs to train the model
    :param batch_size: the batch size per chunk
    :param n_chunks: if the training data set is broken into multiple files, n_chunks must be equal to the number of the
                     files; n_chunks must always be at least >= 1
    :param validation_split: float (0. < x < 1). Fraction of the data to use as held-out validation data
    """
    def load_full_data():
        # get training input and tags as two numpy arrays
        training_input = os.path.join(dp.PROCESSED_PHOTOS_AS_ARRAYS_DIR, 'train.npy')
        training_tags = os.path.join(dp.PROCESSED_PHOTOS_AS_ARRAYS_DIR, 'train_tags.npy')
        yield np.load(training_input), np.load(training_tags)

    if n_chunks == 1:
        # load input and output from 2 files only
        data_generator = load_full_data()
        n_super_epochs = 1
    elif n_chunks > 1:
        # load input and output from multiple files
        data_generator = training_data_chunk_generator(n_chunks)
        n_super_epochs = n_epochs
        n_epochs = 1
    else:
        raise Exception("the number of training files (chunks) must be at least 1")

    histories = defaultdict(list)

    # train model for n_epochs times
    for k in range(n_super_epochs):
        # print info
        print_section_header(
            'TRAINING -- {}/{} super epoch  '.format(k + 1, n_super_epochs) +
            '(data generator is {})'.format(data_generator.__name__)
        )

        for i in range(n_chunks):
            # get training data
            input_data, tags = next(data_generator)
            # train model and save history
            histories[k].append(
                model.fit(
                    x=input_data,
                    y=tags,
                    epochs=n_epochs,
                    batch_size=batch_size,
                    validation_split=validation_split
                ).history
            )
            # free up memory since data is large
            del input_data
            del tags
            gc.collect()

    # save histories
    with open(os.path.join(dp.DATA_DIR, 'histories.p'), 'wb') as h:
        pickle.dump(histories, h)


def evaluate_model(model, batch_size):
    """
    This function evaluates the accuracy of the model given.
    :param model: the model to be evaluated
    :param batch_size: the batch size
    """
    test_data = np.load(os.path.join(dp.PROCESSED_PHOTOS_AS_ARRAYS_DIR, 'test.npy'))
    # test_tags = np.load(os.path.join(dp.PROCESSED_PHOTOS_AS_ARRAYS_DIR, 'test_tags.npy'))

    print_section_header("EVALUATING")
    # model.evaluate(test_data, test_tags, batch_size)
    predicted_tags = model.predict(test_data, batch_size, verbose=1)

    np.save(os.path.join(dp.DATA_DIR, 'predicted_tags.npy'), predicted_tags)


def training_data_chunk_generator(n_chunks, block_size=1):
    """
    This is a generator function that loads image and tag data from "processed_photos_as_arrays" directory
    as numpy arrays. It returns a block of concatenated data chunks in the ascending order of their file names and
    cycles back to the first chunk after all of the data have been yielded. The file names must be train_x and
    train_tags_x, where x is an integer greater than or equal to 0.

    This function also calls the garbage collector for the second and each subsequent next() call on this
    generator.

    Example (n_chunks == 10, block_size == 2):
        1.  yield concat(train_0, train_1) and concat(train_tags_0, train_tags_1)
        2.  yield concat(train_2, train_3) and concat(train_tags_2, train_tags_3)
        .
        .
        .
        10. yield concat(train_8, train_9) and concat(train_tags_8, train_tags_9)
        11. yield concat(train_0, train_1) and concat(train_tags_0, train_tags_1)

    :param n_chunks: number of data chunks available
    :param block_size: number of data chunks to be combined as one block of chunks; default is 1
    :return: a pair of train_X and train_tags_x where x is in itertools.cycle(range(n_chunks))
    """
    block_train = np.empty((0, 224, 224, 3), dtype=np.float16)
    block_tags = np.empty((0, 512), dtype=np.float16)

    for i in cycle(range(0, n_chunks, block_size)):
        # update block_size so that n_chunks of data are processed per super epoch
        block_size = min(n_chunks - i, block_size)

        # load training data chunks as a single block of chunks
        for j in range(i, i + block_size):
            train_file = 'train_' + str(j) + '.npy'
            tags_file = 'train_tags_' + str(j) + '.npy'
            print("concatenating ", train_file, " and ", tags_file, " with block_train and block_tags")
            train_data = np.load(os.path.join(dp.PROCESSED_PHOTOS_AS_ARRAYS_DIR, train_file))
            train_tags = np.load(os.path.join(dp.PROCESSED_PHOTOS_AS_ARRAYS_DIR, tags_file))
            block_train = np.concatenate((block_train, train_data), axis=0)
            block_tags = np.concatenate((block_tags, train_tags), axis=0)

        # the numpy array of stacked training images and tags is a numpy array of their class labels
        yield block_train, block_tags
