# Author:       Irsan Winarto
# Student I.D.: 44316983
# Supervisor:   Prof. Neil Bergmann
# Course:       ENGG4801

import sys, os, gc
import pandas as pd
import numpy as np
import data_processing as dp
import keras.preprocessing.image as K_image
from PIL import Image
from ast import literal_eval
from time import sleep


def resize_shorter_side_and_center_crop(image, size):
    """
    This function re-sizes the PIL image to the specified size by
        (1) scaling based on the shorter dimension of the image
            while maintaining the original aspect ratio, and then
        (2) center-crop the scaled image to the final size.
    WARNING: This function rescaled the original image object passed in.
    :param image: the image to be resized
    :param size: the final size of the image
    :return: a new image (at a different memory location) that has been
             re-sized and center-crop to the specified size
    """
    # convert PIL image to RGB if necessary
    if image.mode != 'RGB':
        image = image.convert('RGB')

    # boiler plate
    image_size = image.size
    width, height = image_size
    smaller_index = 0 if width <= height else 1
    bigger_index = (smaller_index + 1) % 2

    # scale ratio
    ratio = size[smaller_index] / image_size[smaller_index]

    # resize based on shorter side
    new_size = [0] * 2
    new_size[smaller_index] = size[smaller_index]
    new_size[bigger_index] = image_size[bigger_index] * ratio

    # resize while keeping aspect ratio
    image.thumbnail(new_size, Image.ANTIALIAS)

    # update image size
    width, height = image.size

    # left, right, top, down co-ordinates of the image to be cropped
    new_width = size[0]
    new_height = size[1]
    left = int((width - new_width) / 2)
    top = int((height - new_height) / 2)
    right = left + new_width
    bottom = top + new_height

    # center crop the image
    image = image.crop((left, top, right, bottom))

    return image


def subtract_mean_pixel(img, mean_pixels):
    """
    This function converts image into a numpy array and
    subtracts mean pixels (channel-wise) from image.
    :param img: the image to be converted (must have 3 color channels)
    :param mean_pixels: an array of 3 numbers corresponding to 3 color means
    :return: a numpy array representing the image that has been subtracted by
             the mean pixels
    """
    array = np.asarray(img, dtype='float16')

    for i in range(3):
        array[:, :, i] -= mean_pixels[i]

    return array


def subtract_mean_pixel_batch_images(photo_dir, image_list, mean_pixels, batch_info):
    """
    This function calls the subtract_mean_pixel() function on every image
    in photo_dir.
    :param photo_dir: the directory of images to be processed; each image
                      must have 3 color channels
    :param image_list: a list of image files to be processed in the photo_dir
    :param mean_pixels: an array of 3 numbers corresponding to 3 color means
    :param batch_info: a list of 2 values - batch number and number of batches
    :return: a 4-dimensional numpy array of dimension (BATCH_LENGTH X IMG_DIMS)
             consisting of processed images
    """
    preprocessed = []
    for index, image in enumerate(image_list):
        sys.stdout.write(
            "\rMean subtracting (batch {}/{}): {}/{} images".format(
                batch_info[0], batch_info[1], index + 1, len(image_list)
            )
        )
        sys.stdout.flush()

        preprocessed.append(
            subtract_mean_pixel(
                Image.open(os.path.join(photo_dir, image)), mean_pixels
            )
        )

    print()
    return np.array(preprocessed)


def divide_image_list_and_outputs(image_list, outputs, batch_size):
    """
    This function divides the image_list into len(image_list) % batch_size chunks
    + 1 extra chunk if batch_size does not divide len(image_list)
    :param batch_size: the size of an image batch
    :param image_list: the image list to be divided into batches
    :param outputs: this is a list of output tags corresponding to the image_list
    :return: [image_batches, output_batches]
    """
    if type(batch_size) != int or batch_size > len(image_list):
        raise Exception(
            'batch_size must be a positive integer and no greater than ' +
            'len(image_list)'
        )

    if len(image_list) != len(outputs):
        raise Exception('len(image_list) must equal len(outputs)')

    div = len(image_list) // batch_size
    mod = len(image_list) % batch_size
    tot = div + 1 if mod > 0 else div

    image_batches = [
        image_list[batch_size * k: batch_size * (k + 1)]
        for k in range(tot)
    ]

    output_batches = [
        outputs[batch_size * k: batch_size * (k + 1)]
        for k in range(tot)
    ]

    print("number of image batches: {}".format(len(image_batches)))
    print("number of output batches: {}".format(len(output_batches)))

    return image_batches, output_batches


def generate_batch_names(batch_count, file_names):
    """
    This function generates batch names by appending a number to the
    end of each name in file_names. The total number of new file names
    is 2 * batch_count
    :param batch_count: the number of batches
    :param file_names: a namedtuple of base names of training and test files
    :return: [batch_names, output_names]
    """
    batch_names = []
    output_names = []
    ext = '.npy'

    if batch_count > 1:
        for i in range(batch_count):
            batch_names.append(file_names.batch + '_' + str(i) + ext)
            output_names.append(file_names.output + '_' + str(i) + ext)

    elif batch_count == 1:
        batch_names.append(file_names.batch + ext)
        output_names.append(file_names.output + ext)

    else:
        raise  Exception("the number of batches must be at least 1")

    print(batch_names)
    print(output_names)

    return batch_names, output_names


def convert_non_excl_to_excl_one_hot(one_hot_tags):
    """
    This function converts non-mutually exclusive tags in one_hot_tags
    to mutually exclusive tags.
    :param one_hot_tags: the list of non-mutually exclusive tags to be converted;
                         the list must be a numpy array
    :return: a new numpy array such that each row is a list of mutually exclusive tags
             (0.'s and 1.'s) and that only one element in the row is equal to 1.. The
             length of each row is equal to 2**(one_hot_tags.shape[1]) - 1
    """
    def convert_tag_to_num(tag):
        num = 0
        for bit in tag:
            num = (num << 1) | bit
        return num

    def excl_one_hot(number):
        zeros = [0]*(2**one_hot_tags.shape[1] - 1)
        zeros[number - 1] = 1
        return zeros

    return np.array([
        excl_one_hot(convert_tag_to_num(tag))
        for tag in one_hot_tags
    ]).astype(np.int16)


def create_array_datasets(file_names, batch_size):
    """
    This function creates multiple files as .h5 files:
        (1) numpy array batches that stack all of the training images specified in
            processed_train_mapping.csv,
        (2) numpy array batches that stack all of the test images specified in
            processed_test_mapping.csv,
        (3) numpy array batches that stack all of the output tags of the training set
            as found in processed_train_mapping.csv
        (4) numpy arrays that stack all of the output tags of the test set
            as found in processed_test_mapping.csv.

    All files are saved in the data/processed_photos_as_arrays directory
    """
    # ImageNet mean RGB values as given by the authors of VGG-Net
    mean_RGB = [123.68, 116.779, 103.939]

    # create directory
    if not os.path.exists(dp.PROCESSED_PHOTOS_AS_ARRAYS_DIR):
        os.makedirs(dp.PROCESSED_PHOTOS_AS_ARRAYS_DIR)

    # load the relevant dfs
    train_df = pd.read_csv(os.path.join(dp.DATA_DIR, 'processed_train_mapping.csv'))
    test_df = pd.read_csv(os.path.join(dp.DATA_DIR, 'processed_test_mapping.csv'))
    dfs = [train_df, test_df]

    print("Creating training and test numpy data sets")

    # create numpy arrays for all images in the photo list and the tags
    for index, df in enumerate(dfs):
        # get the list of all photos in the data-frame and divide it into batches
        photo_list = list(map(lambda x: str(x) + '.jpg', df.photo_id.tolist()))

        # all the output tags of the data frame to be divided into batches
        one_hot_tags = list(map(literal_eval, df.one_hot_tags.tolist()))
        one_hot_tags = np.array(one_hot_tags, dtype=np.int16)
        one_hot_tags = convert_non_excl_to_excl_one_hot(one_hot_tags)

        # divide images and outputs into batches
        photo_batches, output_batches = divide_image_list_and_outputs(
            photo_list, one_hot_tags, batch_size
        )

        assert len(photo_batches) == len(output_batches), \
            "number of image batches is not equal to number of output batches!!!"

        # generate file names
        batch_names, output_names = generate_batch_names(len(photo_batches), file_names[index])

        # process every batch
        for batch_number, (batch, output) in enumerate(zip(photo_batches, output_batches)):
            # process the image batch as a numpy array
            photo_array = subtract_mean_pixel_batch_images(
                dp.PROCESSED_PHOTOS_DIR, batch, mean_RGB, [batch_number + 1, len(photo_batches)]
            )

            # save photo batch
            f = os.path.join(dp.PROCESSED_PHOTOS_AS_ARRAYS_DIR, batch_names[batch_number])
            np.save(f, photo_array)

            # save output batch
            f = os.path.join(dp.PROCESSED_PHOTOS_AS_ARRAYS_DIR, output_names[batch_number])
            np.save(f, output)

            print("{} {}".format(photo_array.shape, output.shape))

            # free up memory
            del photo_array
            gc.collect()

            if batch_number >= 3 and batch_number % 3 == 0:
                s = 300
                print("Sleeping for {} seconds".format(s))
                sleep(s)


if __name__ == '__main__':
    # IMAGE_DIR = dp.CENTER_CROPPED_299
    IMAGE_DIR = dp.CENTER_CROPPED_299_TEST

    # make a directory for the 299 x 299 photos and make the photos
    if not os.path.exists(IMAGE_DIR):
        print("Making 299 x 299 photos")
        os.makedirs(IMAGE_DIR)

        # for inception V3
        FINAL_SIZE = (299, 299)
        # for mobile net
        # FINAL_SIZE = (224, 224)

        # get the images
        # image_file_names = os.listdir(dp.TRAIN_PHOTO_DIR)
        image_file_names = os.listdir(dp.TEST_PHOTO_DIR)
        try:
            image_file_names.remove('.floyddata')
        except ValueError:
            pass

        # load the images, process them, and then save them into the directory
        for index, image_file_name in enumerate(image_file_names):
            # image_file_path = os.path.join(dp.TRAIN_PHOTO_DIR, image_file_name)
            image_file_path = os.path.join(dp.TEST_PHOTO_DIR, image_file_name)
            # this resizes the image to the target size and it doesn't keep the aspect ratio
            # image = K_image.load_img(image_file_path, target_size=FINAL_SIZE)
            with Image.open(image_file_path) as image:
                image = resize_shorter_side_and_center_crop(image, FINAL_SIZE)
                if image.mode != 'RGB':
                    image = image.convert(mode='RGB')
            image_file_path = os.path.join(IMAGE_DIR, image_file_name)
            # print status
            sys.stdout.write("\r{}/{} - {}".format(index, len(image_file_names), image_file_path))
            sys.stdout.flush()
            image.save(image_file_path)
            image.close()
