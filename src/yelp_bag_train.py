import os
import gc
import sys
import timeit
import pickle
import numpy as np
import data_processing as dp
from sklearn.mixture import GaussianMixture
from sklearn.cluster import KMeans


if __name__ == '__main__':

    N_COMPONENTS = 1024
    print("Learning bag descriptor -- {} components".format(N_COMPONENTS))
    """
    GMM = GaussianMixture(
        n_components=N_COMPONENTS,
        init_params='random',
        reg_covar=1e-5,
        covariance_type='diag',
        verbose=1
    )
    """
    STACKED_FEATURES_FILE = os.path.join(dp.STACKED_FEATURE_VEC_DIR, 'transformed_features_stacked_cc_inc_v3.npy')
    transformed_training = np.load(STACKED_FEATURES_FILE)
    print("Shape of stacked feature vectors: {}".format(transformed_training.shape))
    print("dtype: {}".format(transformed_training.dtype))

    start_time = timeit.default_timer()

    """
    GMM_FILE_PATH = os.path.join(dp.DATA_DIR, 'GMM_512_cc_inc_v3.pkl')

    while True:
        print("_" * 80)
        pickled_GMM = open(GMM_FILE_PATH, 'rb')
        GMM = pickle.load(pickled_GMM)
        pickled_GMM.close()
        GMM = GMM.set_params(warm_start=True, max_iter=50)
        print("Converged: {}".format(GMM.converged_))
        print("Number of iterations: {}".format(GMM.n_iter_))
        print("Lower bound of log likelihood: {}".format(GMM.lower_bound_))
        print("Fitting.....\n")

        GMM.fit(transformed_training)
        print("Time elapsed: {}".format(timeit.default_timer() - start_time))

        pickled_GMM = open(GMM_FILE_PATH, 'wb')
        pickle.dump(GMM, pickled_GMM)
        pickled_GMM.close()

        if GMM.converged_:
            break

        del GMM
        del pickled_GMM
        gc.collect()


    print("Total time to fit GMM: {}".format(timeit.default_timer() - start_time))
    """

    Kmeans = KMeans(
        n_clusters=N_COMPONENTS,
        n_init=1,
        max_iter=700,
        verbose=1,
        n_jobs=1
    )

    print("_" * 80)
    Kmeans.fit(transformed_training)
    print("Total time to fit KMeans {} components: {}".format(N_COMPONENTS, timeit.default_timer() - start_time))

    KMEANS_FILE_PATH = os.path.join(dp.MODEL_DIR, 'KMeans_' + str(N_COMPONENTS) + '_1_cc_inc_v3.pkl')
    print("Saving Kmeans at " + KMEANS_FILE_PATH)
    with open(KMEANS_FILE_PATH, 'wb') as Kmeans_pickle:
        pickle.dump(Kmeans, Kmeans_pickle)
    print("Done saving")
