import os, sys, pickle
import numpy as np
import pandas as pd
import data_processing as dp
from enum import Enum
from sklearn.metrics import matthews_corrcoef
from keras.models import load_model


class Tag(Enum):
    GOOD_FOR_LUNCH = 0
    GOOD_FOR_DINNER = 1
    TAKES_RESERVATIONS = 2
    OUTDOOR_SEATING = 3
    RESTAURANT_IS_EXPENSIVE = 4
    HAS_ALCOHOL = 5
    HAS_TABLE_SERVICE = 6
    AMBIENCE_IS_CLASSY = 7
    GOOD_FOR_KIDS = 8


def convert_one_hot_to_tags(labels):
    """
    This function converts the one-hot labels into tags of integer characters ranging from 0 to 8 inclusive.
    :param labels: a list-like array of numbers; the numbers are expected to be 0 or 1
    :return: a list of tags converted from the one-hot labels
    """
    if len(labels.shape) == 1:
        labels = np.reshape(labels, (1, -1))

    tags = [''] * labels.shape[0]

    for i in range(labels.shape[0]):

        row_tags = [
            str(col_index)
            for col_index, one_hot_label in enumerate(labels[i])
            if one_hot_label == 1
        ]

        tags[i] = ' '.join(row_tags)

    return tags


def get_decision_thresholds(model, input, actual_output):
    """
    This function determines from the range of binary decision thresholds 0.1, 0.15,...,0.8, which one gives the highest
    Matthew's correlation coefficient (MCC) for each label (column) in the output of the given model. The threshold that
    gets the highest score (minimum is -1 and maximum is 1) is the best decision threshold for the label. A negative
    MCC suggests that the model is not a quality predictor for the label that the negative MCC was calculated for.
    :param model: a Keras model with a predict() method
    :param input: the input data whose output array is to be predicted by the model
    :param actual_output: the correct output array for the input array
    :return: a numpy array of decision thresholds for all columns in the output of model.predict()
    """
    # a list of decision thresholds to be tested
    candidates = np.arange(0.1, 0.9, 0.05).astype(np.float32)
    # get the output predictions
    predictions = model.predict(input, verbose=1)
    # an array of best thresholds for the columns of the output
    best_thresholds = np.empty((predictions.shape[1], ), dtype=np.float32)

    # current best Matthew's correlation coefficient
    best_MCC = -1
    # default to the standard practice threshold 0.5 if no MCC is better than -1 (unlikely)
    best_threshold = 0.5
    # for printing (a negative MCC likely indicates poor predictions for the corresponding label / output column)
    best_MCCs = [-1] * predictions.shape[1]

    # for each column/label get the best threshold
    for column in range(actual_output.shape[1]):
        column_predictions = predictions[:, column]
        # test all candidate threshold values to find which one gives the highest MCC
        for threshold in candidates:
            pred = [
                1 if probability > threshold else 0
                for probability in column_predictions
            ]
            MCC = matthews_corrcoef(actual_output[:, column], pred)
            if MCC > best_MCC:
                best_threshold = threshold
                best_MCC = MCC

        best_thresholds[column] = best_threshold
        best_MCCs[column] = best_MCC
        best_MCC = -1
        best_threshold = 0.5

    print()
    print("Best thresholds: {}".format(best_thresholds))
    print("Best MCCs: {}".format(best_MCCs))
    return best_thresholds


if __name__ == '__main__':
    print("Making Yelp predictions")
    model = load_model(os.path.join(dp.MODEL_DIR, 'yelp_model_KMeans_1_cc_inc_v3.h5'))
    model.summary()

    training_input = np.load(os.path.join(dp.TRAINING_SET_DIR, 'concatenated_train_input_KMeans_1_cc_inc_v3.npy'))
    training_labels = np.load(os.path.join(dp.TRAINING_SET_DIR, 'train_labels_KMeans_1_cc_inc_v3.npy'))
    print("Train shape: {}, dtype: {}".format(training_input.shape, training_input.dtype))
    print("labels shape: {}, dtype: {}".format(training_labels.shape, training_labels.dtype))

    # find for each label the best decision threshold in 0.5 increment from 0.1 up to and including 0.8
    thresholds = get_decision_thresholds(model, training_input, training_labels)

    # the test set
    test_data = np.load(os.path.join(dp.TEST_SET_DIR, 'concatenated_test_input_KMeans_1_cc_inc_v3.npy'))
    print("test_data shape: {}, dtype: {}".format(test_data.shape, test_data.dtype))

    # predict the labels
    predictions = model.predict(test_data, batch_size=256, verbose=1)
    print("predictions shape: {}, dtype: {}".format(predictions.shape, predictions.dtype))

    # get the business id's associated to the predictions
    with open(os.path.join(dp.TEST_SET_DIR, 'corresponding_test_business_ids_1.pkl'), 'rb') as business_ids:
        business_ids = pickle.load(business_ids)
    print("business_ids length: {}".format(len(business_ids)))

    # check the lengths are correct
    assert predictions.shape[0] == len(business_ids), \
        "number of predictions {} does not equal number of business id's {}".format(
            predictions.shape[0], len(business_ids)
        )

    # convert the probabilities of the predictions to 1 and 0 labels as decided using the best thresholds
    for column_index in range(predictions.shape[1]):
        column = predictions[:, column_index]
        column[column > thresholds[column_index]] = 1
        column[column <= thresholds[column_index]] = 0

    # check for a sign that too many businesses have no predictions
    count = 0
    for i in range(predictions.shape[0]):
        if np.array_equal(predictions[i], [0] * 9):
            count += 1
    print("Empty count: {}".format(count))

    # convert the 1 and 0 labels into 0 to 8 inclusive tags
    predictions = convert_one_hot_to_tags(predictions)
    print("Converted predictions length: {}".format(len(predictions)))

    # check for a sign that too many businesses have no predictions
    count = 0
    for i in range(len(predictions)):
        if predictions[i] == '':
            count += 1
    print("Empty count: {}".format(count))

    # check the lengths are correct
    assert len(predictions) == len(business_ids), \
        "number of converted predictions {} does not equal number of business id's {}".format(
            predictions.shape[0], len(business_ids)
        )

    # print to spot if there's any unusual predictions
    last_index = np.random.randint(10, len(predictions))
    print("Sample predictions:\n {}".format(predictions[last_index - 10: last_index]))

    # save the predictions as csv for submission
    yelp_predictions = {
        'business_id': business_ids,
        'labels': predictions
    }

    yelp_predictions = pd.DataFrame(yelp_predictions)

    yelp_predictions.to_csv(os.path.join(dp.DATA_DIR, 'yelp_predictions.csv'), index=False)
