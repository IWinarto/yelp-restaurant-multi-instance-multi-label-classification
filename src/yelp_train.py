# Author:       Irsan Winarto
# Student I.D.: 44316983
# Supervisor:   Prof. Neil Bergmann
# Course:       ENGG4801

from keras.models import Model, load_model
from keras.layers import Dense, Activation, Flatten, Dropout, BatchNormalization, Input, merge
from keras.layers.core import Lambda
from keras.callbacks import ModelCheckpoint
from keras.optimizers import SGD, RMSprop, Adagrad, Adadelta, Adam, Adamax, Nadam
from collections import defaultdict
from itertools import cycle
import tensorflow as tf
import data_processing as dp
import numpy as np
import image_processing as imp
import pickle
import argparse
import timeit
import random
import os
import gc


def create_model(input_shape, units, dropout=0.5, n_tags=9):
    """
    This function creates a Keras model with two fully connected layers. The output layer is a sigmoid layer with n_tags
    number of output classes. Each fully connected layer is a series of Dense, BatchNormalization, Activation, and
    Dropout layers. The activation layer is a relu non-linearity, and the dropout rate is 0.5.
    :param input_shape: is the input shape of the model
    :param units: is a list of two positive integer numbers, where the first number is the number of units for the
                  first fully connected layer and the second is the number of units of the second fully connected layer
    :param dropout: the neuron dropout probability; default is 0.5
    :param n_tags: the number of restaurant tags (default is 9) such that the number of units of the output layer is
                   2**n_tags - 1
    :return: a Keras model
    """
    # Input layer
    inputs = Input(shape=input_shape)
    x = inputs

    # Fully connected layers
    for i, n_units in enumerate(units):
        number = str(i + 1)
        x = Dense(units[i], name='FC_' + number)(x)
        x = BatchNormalization(name='BN_' + number)(x)
        x = Activation('relu')(x)
        x = Dropout(dropout, name='drop_' + number)(x)

    # Add multi-label output layer
    predictions = Dense(n_tags, activation='sigmoid', name='output')(x)

    return Model(inputs=inputs, outputs=predictions)


def make_argument_parser():
    """
    This function makes an argument parser for this script.
    :return:
    """
    parser = argparse.ArgumentParser(description='Yelp Restaurant Tagger Trainer')
    parser.add_argument(
        'batch_size',
        type=int,
        help='the model batch size'
    )
    parser.add_argument(
        'n_epochs',
        type=int,
        help='number of training epochs'
    )
    parser.add_argument(
        '-nc', '--n_chunks',
        type=int,
        nargs='?',
        default=1,
        help='the number of training data chunks (.npy) files; default is 1'
    )
    parser.add_argument(
        '-ds', '--dont_save',
        action='store_true',
        default=False,
        help='specify this flag if you don\'t want to save the model'
    )

    return parser.parse_args()


if __name__ == '__main__':
    # training input and output numpy arrays
    train = np.load(os.path.join(dp.TRAINING_SET_DIR, 'sub_train_input_KMeans_1_cc_inc_v3.npy'))
    tags = np.load(os.path.join(dp.TRAINING_SET_DIR, 'sub_train_labels_KMeans_1_cc_inc_v3.npy'))
    validation = np.load(os.path.join(dp.TRAINING_SET_DIR, 'sub_validation_KMeans_1_cc_inc_v3.npy'))
    validation_tags = np.load(os.path.join(dp.TRAINING_SET_DIR, 'sub_validation_labels_KMeans_1_cc_inc_v3.npy'))

    print("Shape of training input: {}, dtype: {}".format(train.shape, train.dtype))
    print("Shape of training output: {}, dtype: {}".format(tags.shape, tags.dtype))
    print("Shape of validation input: {}, dtype: {}".format(validation.shape, validation.dtype))
    print("Shape of validation output: {}, dtype: {}".format(validation_tags.shape, validation_tags.dtype))

    # default model name
    MODEL_NAME = 'yelp_model_KMeans_1_cc_inc_v3.h5'
    MODEL_FILE_PATH = os.path.join(dp.MODEL_DIR, MODEL_NAME)

    # argument parser
    # args = make_argument_parser()

    SHAPE = (train.shape[1], )
    UNITS = [5120]
    model = create_model(SHAPE, UNITS, dropout=0.85)
    model.compile(
        optimizer=Adam(lr=0.0001),
        loss='binary_crossentropy',
        metrics=['accuracy'],
    )

    # print model info
    model.summary()

    # train model and time it
    t0 = timeit.default_timer()
    history = model.fit(train, tags, batch_size=256, epochs=450, validation_data=(validation, validation_tags))
    print("Total time: {}".format(timeit.default_timer() - t0))

    print("Saving model")
    model.save(MODEL_FILE_PATH)

    print("Saving training history")
    with open(os.path.join(dp.MODEL_DIR, MODEL_NAME.split('.')[0] + '_history.pkl'), 'wb') as history_pickle:
        pickle.dump(history.history, history_pickle)

    """
    try:
        train_model(
            model=model,
            n_epochs=args.n_epochs,
            batch_size=args.batch_size,
            n_chunks=args.n_chunks,
            validation_split=0.15/0.85
        )
    finally:
        msg = 'Total time taken to train: {}s'.format(timeit.default_timer() - t0)
        print('_' * len(msg))
        print()
        print(msg)

    # save model
    if not args.dont_save:
        print("Saving model")
        model.save(MODEL_FILE_PATH)

    # evaluate model
    evaluate_model(model, args.batch_size)
    """
